# CryptoWatcher

A simple python script to alert you via email when your cryptocurrency moons or dips.

## Features
- Set 1 hour or 24 hour percent change notifications.
- Set price thresholds notifications based on the your currency price.
- Each alert can be set to notify you only after every x amount of seconds, so you don't get spammed.

### Installation

I used [Python](https://www.python.org/downloads/) 3.7.4 to develop and run this script. 

```bash

git clone url

cd CryptoWatcherFolder/

```

### Configuration

Rename example_config.ini to config.ini

You will need to change the EMAIL section to your email provider info and what email addresses you want to send the alerts to. Smtp code might have to change depending on your email service as well.

Some email providers have exta security around logging in via 3rd party apps, so be aware of that if you face authentication issues. There are multiple sources on how to deal with it.

You can set the currency in the config.ini file.

The crypto_settings.json file is an array of the cryptos you want to watch and keep track of.

```json
        {
            "name": "bitcoin", //Id in coingecko ex. https://www.coingecko.com/en/coins/bitcoin
            "symbol": "BTC",
            "priceAlerts": [ //You can have as many as you want
                {
                    "price": 40000,
                    "direction": 0, // 0 is positive direction and 1 is negative direction
                    "lastTriggered": "", //timestamp when it was last triggered
                    "triggerEvery": 86400, //Amount of seconds to pass before it can notify you again
                    "active": true //Deactivate it, if you don't want to be notified anymore
                },
                {
                    "price": 20000,
                    "direction": 1,
                    "lastTriggered": "",
                    "triggerEvery": 86400,
                    "active": true
                },
                {
                    "change": 5.0, // percent threshold
                    "hour": "1h", // 1h for 1 hour change check or 24h for 24 hour change check
                    "direction": 0,
                    "lastTriggered": "",
                    "triggerEvery": 1,
                    "active": true
                },
                {
                    "change": -5.0,
                    "hour": "1h",
                    "direction": 1,
                    "lastTriggered": "",
                    "triggerEvery": 1,
                    "active": true
                }
            ]
        }
```

### Running the script

```bash

cd CryptoWatcherFolder/

python main.py

```

### Other Info

If you are familiar with [cron](https://en.wikipedia.org/wiki/Cron) jobs or windows task scheduler, then I suggest running this script on a scheduled basis, so you don't have to run it manually.

Congratz, you can now sit back and never be glued to your exchange every minute again!

Powered by [CoinGecko](https://www.coingecko.com/en/api)

Disclaimer: Use at your own risk, this script only provides publicly available information to use and make your own decisions. Always cross-check with other sources.
