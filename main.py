import json
import requests
import smtplib, ssl
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from datetime import datetime
import configparser

config = configparser.ConfigParser()

config.read("config.ini")

sender_email = config["EMAIL"]['sender_email']
receiver_emails = config["EMAIL"]['receiver_emails']
password = config["EMAIL"]['sender_password']

subject = "CryptoWatcher"

message = MIMEMultipart("alternative")
message["From"] = sender_email
message["To"] = receiver_emails

api_url = config["API"]['api_url']

currency = config["LOCALIZATION"]["currency"].lower()

headers = {'content-type': 'application/json'}

cryptoAlerts = {}
sep = ','

dateTimeFormat = "%m/%d/%Y %H:%M:%S"

html = """\
<html>
  <body>
"""

error = False

def alert_triggered(symbol, alert):
    if symbol in cryptoAlerts:
        cryptoAlerts[symbol].append(alert)
    else:
        cryptoAlerts[symbol] = [alert]

try:
    with open("crypto_settings.json", "r+") as file:
        data = json.load(file)
        cryptoToWatch = data["cryptos"]

        for crypto in cryptoToWatch:
            symbol = crypto["symbol"]
            response = requests.get(f"{api_url}/coins/{crypto['name']}?localization=false&community_data=false&developer_data=false&tickers=false", headers=headers)
            if response.status_code != 200:
                html += "<h3> <b>error occured while trying to retrieve gecko data</h3>"
                html += "<br>"
                continue
            json_data = json.loads(response.text)
            market_data = json_data["market_data"]
            price = market_data["current_price"][currency]
            volume = market_data["total_volume"][currency]
            percent_change_1h = market_data["price_change_percentage_1h_in_currency"][currency]
            percent_change_24h = market_data["price_change_24h_in_currency"][currency]

            for alert in crypto["priceAlerts"]:
                if alert["active"] == True and alert["triggerEvery"] != "" and (alert["lastTriggered"] == "" or (alert["lastTriggered"] != ""
                 and (datetime.now() - datetime.strptime(alert["lastTriggered"], dateTimeFormat)).total_seconds() > alert["triggerEvery"])):
                    if alert["direction"] == 0:
                        if "price" in alert and price > alert["price"]:
                            alert_triggered(symbol, alert)
                            alert["lastTriggered"] = datetime.now().strftime(dateTimeFormat)
                            html += f"{symbol} price is above {alert['price']} at {price}"
                            html += "<br>"
                        else:
                            if "change" in alert and ((alert["hour"] == "1h" and percent_change_1h > alert["change"]) or (alert["hour"] == "24h" and percent_change_24h > alert["change"])):
                                alert_triggered(symbol, alert)
                                if alert["hour"] == "1h":
                                    html += f"{symbol} price is up {percent_change_1h}% in the past {alert['hour']}"
                                else:
                                    html += f"{symbol} price is up {percent_change_24h}% in the past {alert['hour']}"
                                alert["lastTriggered"] = datetime.now().strftime(dateTimeFormat)
                                html += "<br>"
                    else:
                        if "price" in alert and price < alert["price"]:
                            alert_triggered(symbol, alert)
                            alert["lastTriggered"] = datetime.now().strftime(dateTimeFormat)
                            html += f"price is below {alert['price']} at {price}"
                        else:
                            if "change" in alert and ((alert["hour"] == "1h" and percent_change_1h < alert["change"]) or (alert["hour"] == "24h" and percent_change_24h < alert["change"])):
                                alert_triggered(symbol, alert)
                                if alert["hour"] == "1h":
                                    html += f"{symbol} price is down {percent_change_1h}% in the past {alert['hour']}"
                                else:
                                    html += f"{symbol} price is down {percent_change_24h}% in the past {alert['hour']}"
                                html += "<br>"
                                alert["lastTriggered"] = datetime.now().strftime(dateTimeFormat)
        file.seek(0)  # rewind
        json.dump(data, file, indent=4)
        file.truncate()

except Exception as e:
    print(e)
    error = True
    html += "<p>" + str(e) + "</p>"


html += """\
      </body>
</html>
"""

if len(cryptoAlerts) > 0 or error == True:
    if error == True:
        subject += ": Error Occured!"
    else:
        if len(cryptoAlerts) > 0:
            subject += f": {sep.join(cryptoAlerts.keys())} triggered price threshold"

    message["Subject"] = subject

    part = MIMEText(html, "html")

    message.attach(part)

    context = ssl.create_default_context()
    with smtplib.SMTP_SSL(config["EMAIL"]["smtp_host"], config["EMAIL"]["smtp_port"], context=context) as server:
        server.ehlo()
        server.login(sender_email, password)
        server.sendmail(
            sender_email, receiver_emails, message.as_string()
        )
else:
    print("No alerts triggered")